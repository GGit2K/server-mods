#!/usr/bin/env bash

#not distribution ready!
#more mods here: https://gist.github.com/CommandLeo/926dd2bd6ff9b9621b33076ff5f09c3b

while true; do
    read -p "Do you want to run this skript? (Y/N): " answer
    case $answer in
    [Yy]* ) break;;
    [Nn]* ) exit;;
    * ) echo "Please answer Y or N.";;
    esac
done

DOWNLOADER="aria2c -x4"
DL_OUT_FLAG="-o"
JAVA_PATH="/usr/lib/jvm/java-19-openjdk/bin/java"
JAVA_FLAGS="-Xmx4G"
MC_FLAGS="-nogui"

$DOWNLOADER -o fabric.jar "https://meta.fabricmc.net/v2/versions/loader/1.19/0.14.14/0.11.1/server/jar"

mkdir mods

$DOWNLOADER $DL_OUT_FLAG mods/fabricapi.jar "https://cdn.modrinth.com/data/P7dR8mSH/versions/0.58.0%2B1.19/fabric-api-0.58.0%2B1.19.jar" &

$DOWNLOADER $DL_OUT_FLAG mods/servux.jar "https://media.forgecdn.net/files/3879/398/servux-fabric-1.19.0-0.1.0.jar" &
$DOWNLOADER $DL_OUT_FLAG mods/worldedit.jar "https://media.forgecdn.net/files/3903/74/worldedit-mod-7.2.11-beta-02-dist.jar" &


$DOWNLOADER $DL_OUT_FLAG mods/lithium.jar "https://cdn.modrinth.com/data/gvQqBUqZ/versions/mc1.19-0.8.1/lithium-fabric-mc1.19-0.8.1.jar" &
$DOWNLOADER $DL_OUT_FLAG mods/starlight.jar "https://cdn.modrinth.com/data/H8CaAYZC/versions/1.1.1%2B1.19/starlight-1.1.1%2Bfabric.ae22326.jar" &

$DOWNLOADER $DL_OUT_FLAG mods/carpet.jar "https://github.com/gnembon/fabric-carpet/releases/download/1.4.79/fabric-carpet-1.19-1.4.79+v220607.jar" &
$DOWNLOADER $DL_OUT_FLAG mods/carpetTISadd.jar "https://cdn.modrinth.com/data/jE0SjGuf/versions/mc1.19-v1.38.0/carpet-tis-addition-mc1.19-v1.38.0.jar" &
$DOWNLOADER $DL_OUT_FLAG mods/carpetExtra.jar "https://github.com/gnembon/carpet-extra/releases/download/1.4.79/carpet-extra-1.19-1.4.79.jar" &
#$DOWNLOADER $DL_OUT_FLAG mods/carpetEssentialAddons.jar "https://github.com/Super-Santa/EssentialAddons/releases/download/1.1.5/essentialaddons-1.19.2-1.1.5.jar" &
$DOWNLOADER $DL_OUT_FLAG mods/carpetFixes.jar "https://cdn.modrinth.com/data/7Jaxgqip/versions/v1.11.2/carpet-fixes-1.19-1.11.2.jar" &
#$DOWNLOADER $DL_OUT_FLAG mods/carpetautoCraftingTable.jar "https://github.com/gnembon/carpet-autoCraftingTable/releases/download/1.4.91/carpet-autocraftingtable-1.19.3-1.4.91.jar" &
# carpet autocrafting & essentialaddons prohibit the execution of the main minecraft server core

$DOWNLOADER $DL_OUT_FLAG mods/syncmatica.jar "https://cdn.modrinth.com/data/bfneejKo/versions/GivVRSry/syncmatica-1.18.2-0.3.8.jar" &
$DOWNLOADER $DL_OUT_FLAG mods/krypton.jar "https://cdn.modrinth.com/data/fQEb0iXm/versions/0.2.0/krypton-0.2.0.jar" &
$DOWNLOADER $DL_OUT_FLAG mods/ferritecore.jar "https://cdn.modrinth.com/data/uXXizFIs/versions/5.0.0-fabric/ferritecore-5.0.0-fabric.jar" 


while true; do
    read -p "By replying with 'Yes' you are indicating your agreement to the EULA (https://account.mojang.com/documents/minecraft_eula)(Y/N): " reply
    case $reply in
    [Yy]* ) break;;
    [Nn]* ) exit;;
    * ) echo "Please answer Y or N.";;
    esac
done

echo "eula=true" > eula.txt

echo "#!/bin/sh" > start.sh
echo "	$JAVA_PATH $JAVA_FLAGS -jar fabric.jar $MC_FLAGS" >> start.sh


chmod +x start.sh

echo "Run start.sh to start the server, this script can now be deleted."

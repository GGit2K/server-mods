## 3.Synchronisierung

- [ ] parallele Arbeitsabläufe:
- hängen zeitlich nicht von einander ab
- können daher wirklich zeitgleich durchgeführt werden

- [ ] nebenläufige Arbeitsabläufe:
- zeitliche Abfolge/Reihenfolge der Abläufe festgelegt

- [ ] kritischer Abschnitt:
- Abschnitt eines Vorgangs, beidem gemeinsam, gleichzeitig mit anderen Vorgängen auf bestimmte Betriebsmittel zugegriffen werden könnte.

- [ ] wechselseitiger Ausschluss:
- ein Vorgang reserviert in der Zeit, inder der kritische Abschnitt abgearbeitet wird, die Betriebsmittel für sich

Synchronisierung:
- Nebenläufige Vorgänge stimmen ihre Ressourcen/Abläufe zeitlich mit einander ab, um Konflikte zu vermeiten

Edsger Wybe Dijkstra: Konzept des Semaphors

Semaphoren:
- Objekte mit den Attributen `Zähler` und `Warteschlange` und mit den Methoden `down()` und `up()`

    - [ ] Zähler:
    - nicht negative ganze Zahl (ℕ0) 
    
    - [ ] Wechselbedingter Ausschluss:
    - nur Werte `0` und `1` 
    
    - [ ] Warteschlange:
    - Verwaltet die Objekte und oder Geräte, die auf die Freigabe warten

- `down()` bewirkt, dass der Zähler falls er nicht Null `0` ist und um eins `1` verringert wird.
    - der Vorgang kann dann in den kritischen Abschnitt eintreten
    - ist der Wert Null `0`, so bleibt er unverändert und der auftreffende Vorgang wird in die Warteschlange eingereiht
- `up()` bewirkt, dass der Wert vom Zähler um eins `1` erhöht wird, ein Vorgang aus der Warteschlange herausgenommen, und wieder aktiviert wird
- Beide Methoden dürfen nicht durch andere Vorgänge unterbrochen werden. s.149


Verklemmung (deadlock):
- keiner von mehreren nebenläufigen Prozessen/Vorgängen kann fortgesetzt werden. Verklemmungen entstehen, wenn die folgenden vier Bedingungen gleichzeitig erfüllt sind:
    
    1. Wechselseitiger Ausschluss
    - jedes Betriebsmittel wird von einem Prozess belegt oder ist frei
    2. Belegungs- und Wartebedingung
    - Ein Vorgang, der bereits Betriebsmittel belegt, kann weitere anfordern
    3. Ununterbrechbarkeitsbedingung
    - Betriebsmittel, die von einem Vorgang belegt werden, können diesem nicht ohne Folgen entzogen werden 
    4. Zyklische Wartebedingung
    - es existiert ein zyklisch[er] [V]orgänge, sodass jeder Vorgang ein Betriebsmittel anfordert, dass von einem anderen belegt ist
- Strategien für den Umgang mit Verklemmungen
    1. Behebung
    - Einem Vorgang wird ein an der Verklemmung beteiligtes Betriebsmittel entzogen oder der Vorgang ist insgesammt abgebrochen
    2. Verhinderung
    - Vorrausschauende Zuteilung der Betriebsmittel, sodass zumindest ein Prozess zum Ende kommt und die von ihm belegten Betriebsmittel wieder freigeben kann
    3. Vermeidung
    - mindestens eins der Verklemmungsbedingungen wird außer Kraft gesetzt






